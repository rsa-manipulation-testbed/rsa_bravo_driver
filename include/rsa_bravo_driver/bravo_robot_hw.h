// Copyright 2023 University of Washington, Oregon State University
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
// may be used to endorse or promote products derived from this software without
// specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

// ROS
#include <ros/ros.h>
#include <rosparam_shortcuts/rosparam_shortcuts.h>
#include <sensor_msgs/JointState.h>
#include <urdf/model.h>

// ROS Control
#include <controller_manager/controller_manager.h>
#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/robot_hw.h>
#include <joint_limits_interface/joint_limits.h>
#include <joint_limits_interface/joint_limits_interface.h>
#include <joint_limits_interface/joint_limits_rosparam.h>
#include <joint_limits_interface/joint_limits_urdf.h>

#include "libbpl_protocol/async_client.h"
#include "libbpl_protocol/io_service_thread.h"

// Enable service
#include "rsa_bravo_driver/bravo_interface.h"
#include "rsa_bravo_msgs/EnableBravo.h"

namespace rsa_bravo_driver {

using libbpl_protocol::Packet;

const unsigned int NUM_JOINTS = 7;

struct JointState {
  float angular_position_;
  float angular_velocity_;
};

/// \brief Hardware interface for a robot
class BravoRobotHw : public hardware_interface::RobotHW {
 public:
  enum class BravoState { Uninitialized, Initialized, Unknown };

  enum class EnableRequest { None, EnableRequested, DisableRequested };

  /// Used to track the type of controller currently enabled
  ///
  /// Determines whether positions of velocities are sent to the
  /// arm / gripper
  enum class ControlMode {
    NoController,
    PositionController,
    VelocityController
  };

  /**
   * \brief Constructor
   * \param nh - Node handle.
   * \param pnh - Private node handle.
   * \param urdf_model - optional pointer to a parsed robot model
   */
  BravoRobotHw(ros::NodeHandle& nh, ros::NodeHandle& pnh,
               urdf::Model* urdf_model = NULL);

  /** \brief Destructor */
  virtual ~BravoRobotHw();

  /** \brief The init function is called to initialize the RobotHW from a
   *         non-realtime thread.
   *
   * Initialising a custom robot is done by registering joint handles
   * (\ref hardware_interface::ResourceManager::registerHandle) to hardware
   * interfaces that group similar joints and registering those individual
   * hardware interfaces with the class that represents the custom robot
   * (derived from this \ref hardware_interface::RobotHW)
   *
   * \note Registering of joint handles and interfaces can either be done in the
   * constructor or this \ref init method.
   *
   * \returns True if initialization was successful
   */
  virtual bool init();

  /** \brief Read data from the robot hardware.
   *
   * The read method is part of the control loop cycle (\ref read, update, \ref
   * write) and is used to populate the robot state from the robot's hardware
   * resources (joints, sensors, actuators). This method should be called before
   * controller_manager::ControllerManager::update() and \ref write.
   *
   * \note The name \ref read refers to reading state from the hardware.
   * This complements \ref write, which refers to writing commands to the
   * hardware.
   *
   * Querying WallTime inside \ref read is not realtime safe. The parameters
   * \p time and \p period make it possible to inject time from a realtime
   * source.
   *
   * \param time The current time
   * \param period The time passed since the last call to \ref read
   */
  virtual void read(const ros::Time& time,
                    const ros::Duration& period) override;

  /** \brief Write commands to the robot hardware.
   *
   * The write method is part of the control loop cycle (\ref read, update, \ref
   * write) and is used to send out commands to the robot's hardware resources
   * (joints, actuators). This method should be called after \ref read and
   * controller_manager::ControllerManager::update.
   *
   * \note The name \ref write refers to writing commands to the hardware.
   * This complements \ref read, which refers to reading state from the
   * hardware.
   *
   * Querying WallTime inside \ref write is not realtime safe. The parameters
   * \p time and \p period make it possible to inject time from a realtime
   * source.
   *
   * \param time The current time
   * \param period The time passed since the last call to \ref write
   */
  virtual void write(const ros::Time& time, const ros::Duration& period);

  /// \brief Called when controllers switch
  ///
  /// \param start_list List of controllers which are being started.
  /// \param stop_list List of controllers which are being stopped.
  ///
  virtual void doSwitch(
      const std::list<hardware_interface::ControllerInfo>& start_list,
      const std::list<hardware_interface::ControllerInfo>& stop_list);

 protected:
  /** \brief Get the URDF XML from the parameter server */
  virtual void loadURDF(const ros::NodeHandle& nh, std::string param_name);

  // Short name of this class
  std::string name_;

  // Local copies of node handles for internal use
  ros::NodeHandle nh_, pnh_;

  // Hardware interfaces
  // hardware_interface::JointStateInterface gives read access to all joint
  // values without conflicting with other controllers.
  hardware_interface::JointStateInterface joint_state_interface_;

  // hardware_interface::PositionJointInterface inherits from
  // hardware_interface::JointCommandInterface and is used for reading and
  // writing joint positions. Because this interface reserves the joints for
  // write access, conflicts with other controllers writing to the same joints
  // might occur. To only read joint velocities, avoid conflicts using
  // hardware_interface::JointStateInterface.
  hardware_interface::PositionJointInterface position_joint_interface_;
  hardware_interface::VelocityJointInterface velocity_joint_interface_;

  // Configuration
  std::vector<std::string> joint_names_;
  std::size_t num_joints_;
  urdf::Model* urdf_model_;

  std::string arm_ip_address_;
  unsigned int arm_udp_port_;

  // If monitor mode is true, the driver watchdog is disabled and
  // a ReadOnlyBravoInterface is created.
  //
  // This mode may be paired with the listen-only port in the
  // bpl_watchdogd
  //
  bool monitor_mode_;

  // Data member array to store the controller commands to be sent to the
  // robot's resources (joints, actuators)
  double joint_position_commands_[NUM_JOINTS];
  double joint_velocity_commands_[NUM_JOINTS];

  // Data member arrays to store the latest state of the robot's resources
  // (joints, sensors) These values are filled in the read() method and
  // are registered to the joint_state_interface_ of type
  // hardware_interface::JointStateInterface.
  double joint_positions_[NUM_JOINTS];
  double joint_velocities_[NUM_JOINTS];
  double joint_efforts_[NUM_JOINTS];

  std::shared_ptr<BravoInterface> bravo_interface_;
  void connectToArm(void);
  std::chrono::time_point<std::chrono::system_clock> last_arm_init_;

  BravoState state_;
  ControlMode arm_control_mode_, gripper_control_mode_;

  // Control loop
  void update(const ros::TimerEvent& e);
  ros::Timer control_loop_timer_;
  double control_loop_rate_;
  // ros::Duration elapsed_time_;
  // static const double ControlLoopTimerPeriod;

  boost::shared_ptr<controller_manager::ControllerManager> controller_manager_;

  // the enabled_ variable is set by the EnableBravo service,
  // and enables the write() functionality.
  bool enableSrvCallback(rsa_bravo_msgs::EnableBravoRequest& req,
                         rsa_bravo_msgs::EnableBravoResponse& res);
  ros::ServiceServer enable_bravo_server_;

  EnableRequest enable_requested_;

  // heartbeat frequency to query sensors.
  double arm_poll_freq_;

  // Joint limits interfaces - Saturation
  joint_limits_interface::PositionJointSaturationInterface
      pos_jnt_sat_interface_;

  joint_limits_interface::VelocityJointSaturationInterface
      vel_jnt_sat_interface_;

  // Copy of limits, in case we need them later in our control stack
  std::vector<double> joint_position_lower_limits_;
  std::vector<double> joint_position_upper_limits_;

  void registerJointPositionLimitsFromURDF(
      const hardware_interface::JointHandle& joint_handle_position,
      std::size_t joint_id);

  double gripper_current_lower_lim_;
  double gripper_current_upper_lim_;

  double deadband_epsilon_rad_;

  int joint_expiration_microseconds_;  // max allowed time since last update

};  // class BravoRobotHw

}  // namespace rsa_bravo_driver
