//
// Copyright 2023 University of Washington, Oregon State University
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
// may be used to endorse or promote products derived from this software without
// specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include <ros/ros.h>

#include "libbpl_protocol/async_client.h"
#include "libbpl_protocol/io_service_thread.h"
#include "libbpl_protocol/packet_counter.h"

namespace rsa_bravo_driver {

using libbpl_protocol::Packet;

class BravoInterface {
 public:
  static const unsigned int NUM_JOINTS = 7;

  virtual ~BravoInterface();

  bool isEnabled() const { return is_enabled_; }

  double position(size_t i) const { return joint_positions_[i]; }
  double velocity(size_t i) const { return joint_velocities_[i]; }
  double effort(size_t i) const { return joint_efforts_[i]; }

  // ~~~ Pure virtual functions which handle commands *to* the arm. ~~~
  //
  // \todo{amarburg} This set of functions could probably be
  //                 optimized into a smaller set of core abstractions
  virtual void send(const libbpl_protocol::Packet &v) = 0;

  virtual bool areCommsGood(void) const = 0;

  // ~~ The following all **must** call send() for the ReadOnly / ReadWrite
  // structure to work
  //
  virtual void enable();
  virtual void disable();

  virtual void halt_bravo();

  /// Send a position command to an actuator.  Position is
  /// in rad for angular joints and mm for linear joints.
  virtual void sendPosition(uint8_t device_id, float position);

  /// Send a velocity command to an actuator.  Velocity is in
  /// rad/sec for angular joints and mm/sec for linear joints.
  virtual void sendVelocity(uint8_t device_id, float velocity);

  virtual void setCurrentLimits(uint8_t device_id, double lower_limit,
                                double upper_limit);

 protected:
  BravoInterface(ros::NodeHandle &nh, const std::string &ipAddress,
                 const int port);

  void packetRxCallback(libbpl_protocol::Packet packet);

  libbpl_protocol::IoServiceThread io_thread_;
  std::shared_ptr<libbpl_protocol::AsynchronousClient> bpl_client_;

  ros::Publisher mode_pub_;
  ros::Publisher position_pub_, velocity_pub_, current_pub_, temperature_pub_,
      voltage_pub_;
  ros::Publisher position_limits_pub_, velocity_limits_pub_,
      current_limits_pub_;

  void statisticsCallback(const ros::TimerEvent &e);
  ros::Timer statistics_timer_;

  bool is_enabled_;

  // these are updated by callbacks and periodically copied into
  // joint_positions_, etc.
  std::array<double, NUM_JOINTS> joint_positions_;
  std::array<double, NUM_JOINTS> joint_velocities_;
  std::array<double, NUM_JOINTS> joint_efforts_;

  // timestamps of last update, microseconds
  std::array<long long, NUM_JOINTS> joint_update_times_;

  libbpl_protocol::PacketCounter<NUM_JOINTS> packets_from_arm_;

 private:
  void publishBravoFloatMsg(ros::Publisher &pub, int joint, float val);
  void publishBravoLimitsMsg(ros::Publisher &pub,
                             libbpl_protocol::Packet packet);
};

class ReadOnlyBravoInterface : public BravoInterface {
 public:
  ReadOnlyBravoInterface(ros::NodeHandle &nh, const std::string &ipAddress,
                         const int port);

  virtual ~ReadOnlyBravoInterface() {}

  virtual void send(const libbpl_protocol::Packet &v) override { ; }

  virtual bool areCommsGood(void) const override { return true; }

  ReadOnlyBravoInterface(void) = delete;
  ReadOnlyBravoInterface(const ReadOnlyBravoInterface &) = delete;
};

class ReadWriteBravoInterface : public BravoInterface {
 public:
  virtual ~ReadWriteBravoInterface() {}

  void send(const libbpl_protocol::Packet &v) override;

  bool areCommsGood(void) const override;

 protected:
  // Private constructor; BravoInterface is not directly instantiable
  // Create a BravoInterfaceHeartbeat or BravoInterfacePolled
  // (it's effectively pure virtual but we don't have any pure virtual
  // functions)
  ReadWriteBravoInterface(ros::NodeHandle &nh, const std::string &ipAddress,
                          const int port, long long joint_expiration);

 private:
  double joint_expiration_microseconds_;
};

//===================================================================
//
// This version of BravoInterface uses the Heartbeat function to
// get state from the arm (the original behavior)
//
class BravoInterfaceHeartbeat : public ReadWriteBravoInterface {
 public:
  BravoInterfaceHeartbeat(ros::NodeHandle &nh, const std::string &ipAddress,
                          const int port, int heartbeat_freq,
                          long long joint_expiration);
};

//===================================================================
//
// This version of BravoInterface uses a ROS timer at PollRate to
// poll the current state of the arm.
//
class BravoInterfacePolled : public ReadWriteBravoInterface {
 public:
  // At present these are constants
  // \enhancement{} expose as parameters
  static const float PollMetadataPeriod;
  static const float PollLimitsPeriod;

  BravoInterfacePolled(ros::NodeHandle &nh, const std::string &ipAddress,
                       const int port, int heartbeat_freq,
                       long long joint_expiration);

 private:
  void pollJointsCallback(const ros::TimerEvent &e);
  void pollMetadataCallback(const ros::TimerEvent &e);
  void pollLimitsCallback(const ros::TimerEvent &e);

  ros::Timer poll_joints_timer_;
  ros::Timer poll_metadata_timer_;
  ros::Timer poll_limits_timer_;
};

}  // namespace rsa_bravo_driver
