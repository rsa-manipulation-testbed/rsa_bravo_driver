#!/usr/bin/env python3
#
# Copyright 2023 University of Washington, Oregon State University
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
# may be used to endorse or promote products derived from this software without
# specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""ROS/bpl_passthrough/scripts/request_joint_positions.py"""

import time
from typing import Optional

import rospy
from bpl_msgs.msg import Packet
from bplprotocol import BPLProtocol, PacketID
from std_msgs.msg import Float32

"""
Measure the latency to contact Bravo arm.

Requests the position of joint 1 (jaw), then publishes and prints the time that it takes to get a response.

Upon receipt of a response, it waits a bit then sends another message.

Filters the time with a moving average filter.

To use this script, first `roslaunch bpl_passthrough serial_passthrough.launch` (or `udp_passthrough.launch`).
Then `rosrun hannahs_bravo_driver test_udp_serial.py`.

You can plot out the time that it takes using, e.g., rqt_plot.
"""

# class MovingAvg:
#     def __init__(self):
#         self.q = deque(maxlen=20)

#     def update(self, x):
#         self.q.append(x)
#         return sum(self.q)/len(self.q)

sent_time: Optional[float] = None
# filt = MovingAvg()


def receive_packet(packet: Packet):
    print("Received a packet")

    global sent_time, filt, tx_publisher, latency_publisher

    device_id = packet.device_id
    packet_id = packet.packet_id
    data = bytearray(packet.data)

    if packet_id == PacketID.POSITION and device_id == 1:  # rotating base position
        rec_time = time.time_ns()
        # time_took = filt.update((rec_time - sent_time)/1e6)
        if sent_time is None:
            time_took = 0.0
        else:
            time_took = (rec_time - sent_time) / 1e6
        print(f"Time took: {time_took} ms")
        latency_publisher.publish(Float32(time_took))

        position = BPLProtocol.decode_floats(data)[0]
        print("Position Received: {} - {}".format(device_id, position))

        time.sleep(0.15)  # wait a bit for all the position messages to get through

        print("------- REQUESTING A PACKET ------")
        tx_publisher.publish(request_packet)
        sent_time = time.time_ns()


if __name__ == "__main__":
    tx_publisher = rospy.Publisher("tx", Packet, queue_size=100)
    latency_publisher = rospy.Publisher("latency", Float32, queue_size=100)

    rospy.init_node("request_joint_position_script")

    rx_subscriber = rospy.Subscriber("rx", Packet, receive_packet)

    request_packet = Packet(0x01, PacketID.REQUEST, [PacketID.POSITION])

    print("------- REQUESTING A PACKET ------")
    tx_publisher.publish(request_packet)
    sent_time = time.time_ns()

    rospy.spin()
