# rsa_bravo_driver

This is a ROSControl driver for the Reach Bravo 6-DOF underwater robot arm. It
uses [libbpl_protocol](https://gitlab.com/rsa-manipulation-testbed/libbpl_protocol) to read and write joint angles on the Bravo arm, and provides a [RobotHW](https://github.com/ros-controls/ros_control/wiki/hardware_interface) interface compatible with [ROS Control](https://github.com/ros-controls/ros_control/wiki/The-ros_control-Wiki).

It uses libbpl_protocol's [AsynchronousClient](https://gitlab.com/rsa-manipulation-testbed/libbpl_protocol/-/blob/main/include/libbpl_protocol/async_client.h) class to communicate with the arm over UDP, using the libbpl_protocol watchdog to connect via`127.0.0.1`. The arm can also be reached via `192.168.2.3`, the default Bravo configuration from the factory. This circumvents the watchdog, which is responsible for monitoring connections to/from the arm and is not recommended.

# Installation
This ROS package is installed using catkin, typically as part of the [`raven_manip_sw`](https://gitlab.com/apl-ocean-engineering/raven/manipulation/raven_manip_sw) meta-repo.

See the raven_manip_sw
[README](https://gitlab.com/apl-ocean-engineering/raven/manipulation/raven_manip_sw/-/blob/main/README.md?ref_type=heads)
for the specific commands.

# Using rsa_bravo_driver

To use the ROSControl driver to provide a JointTrajectoryController to a ROS
system, put the following in a launch file.

```xml
<!-- URDF of the Bravo Robot -->
<arg name="model" default="$(find bravo_description)/urdf/bravo_table.xacro"/>

<!-- Load the URDF onto the parameter server -->
<param name="bravo/robot_description" command="$(find xacro)/xacro $(arg model)"/>

<!-- Load bpl_watchdog -->
<node name="watchdog_node" pkg="libbpl_protocol" type="bpl_watchdogd" output="screen"/>

<group ns="bravo">
    <node name="rsa_bravo_driver" pkg="rsa_bravo_driver" type="rsa_bravo_driver"
        output="screen" respawn="false">
    <!-- Load driver config to the parameter server -->
    <rosparam command="load" file="$(find rsa_bravo_driver)/config/bravo.yaml"/>
    </node>

    <!-- Load controller config to the parameter server -->
    <rosparam command="load" file="$(find rsa_bravo_driver)/config/bravo_control.yaml"/>

</group>

<!-- Spawn controllers using the rsa_bravo_driver RobotHW interface-->
<node name="controller_spawner" pkg="controller_manager" type="spawner" respawn="false"
    output="screen" ns="bravo"
    args="joint_state_controller arm_position_controller hand_position_controller"/>

<!-- Copy the ROS /joint_states topic to tf for visualization -->
<node name="robot_state_publisher" pkg="robot_state_publisher" type="robot_state_publisher"
        ns="bravo"  respawn="true" output="screen"/>
```

To enable the driver, issue the command
```
rosservice call /bravo/enable_bravo 1
```

When the driver is enabled, the following happens:
1. a `libbpl_protocol::AsynchronousClient::**enable()**` command sends a
   MODE_STANDBY packet to the arm.
2. a `libbpl_protocol::AsynchronousClient::**sendPosition()**` commands are
   streamed to the arm based on the driver's `joint_position_commands_[i]`
   vector.

When the driver is disabled (or at start), the following happends:
1. a `libbpl_protocol::AsynchronousClient::**disable()**` command sends a
   MODE_DISABLE packet to the arm.
2. a `libbpl_protocol::AsynchronousClient::**sendPosition()**` commands are no
   longer streamed to the arm.

> :warning: **NB!**: the `joint_position_commands_` vector is initialized to zero
> prior to being continuously populated by a JointTrajectoryController. If the
> driver is enabled while a JointTrajectoryController is not active, rapid
> motion may occur.


# Using joy_trigger_jaw.py

This package also includes a utility node to set the position of the jaw based
on joystick triggers, to complement the joystick functionality provided by
`teleop_twist_joy`. It is invoked like:

```xml
<!-- Start a node to turn Xbox controls into a geometry_msgs::Twist -->
<include file="$(find teleop_twist_joy)/launch/teleop.launch">
    <arg name="config_filepath" value="$(find bravo_moveit_config)/config/xbox_controller.yaml" />
</include>

<!-- Start a node to control the gripper with the controller triggers -->
<node pkg="rsa_bravo_driver" type="joy_trigger_jaw.py" name="joy_trigger_jaw"
    output="screen" launch-prefix="xterm -hold -e "/>
```

Using our `xbox_controller.yaml` file in the `bravo_moveit_config` repo, this maps the left and right triggers to the jaw open/close controls.

# Bravo Arm best practices for UDP

We did extensive testing with our 1st-Generation Bravo arm to achieve (relatively) reliable UDP communications.  Note much of this testing was through the relatively complex, switched network of our ROV and some UDP dropout issues may be due to our network configuration.

* Connect to the "Compute" module at `192.168.2.4` rather than the "Base" at `192.168.2.3`.   I am not sure what specific impact this has architecturally; however one difference is the the Comptue module may package multiple BPL packets into a single UDP packet.  `libbpl_protocol` will decode these packets correctly.

* Use `BravoInterfacePolled`.  The heartbeat functionality lead to some unpredictable behavior including spontaneous reboots; notably the OEM-provided ReachControl software polls the hardware and does not use heartbeat.

* Use network partitioning (VLANs or similar) to isolate the arm network segment from other traffic (this might be specific to our configuration)

* Even with this we still had infrequent dropouts of ~0.6-1.0 seconds where the arm produces no UDP packets.   We have never had them happen while the arm is in motion, so it's not clear if this is dangerous.

# LICENSE

This code was written by students and employees of the University of Washington and Oregon State University.  It is released under the [BSD 3-Clause license.](LICENSE)
