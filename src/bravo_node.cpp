// Copyright 2023 University of Washington, Oregon State University
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
// may be used to endorse or promote products derived from this software without
// specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <controller_manager/controller_manager.h>
#include <ros/ros.h>

#include "rsa_bravo_driver/bravo_robot_hw.h"

int main(int argc, char **argv) {
  // Initialize the ROS node
  ros::init(argc, argv, "rsa_bravo_driver");
  ros::NodeHandle nh;
  ros::NodeHandle pnh("~");

  // Create an instance of your robot so that this instance knows about all
  // the resources that are available.
  rsa_bravo_driver::BravoRobotHw bravo(nh, pnh);
  if (!bravo.init()) {
    ROS_FATAL("Failed to initialize Bravo driver");
    exit(-1);
  }

  // Note this requires at least two threads ... for the ::update() loop (on a
  // ROS timer) and another for service calls.  Some controller_manager calls
  // may block
  ros::AsyncSpinner spinner(0);
  spinner.start();

  ros::waitForShutdown();

  return 0;
}
