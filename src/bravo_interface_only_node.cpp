// Contains a ROS node which implements a minimal wrapper around a
// BravoInterface.  Does not start the associated ROS control
// classrd
//
//
// Copyright 2023 University of Washington, Oregon State University
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
// may be used to endorse or promote products derived from this software without
// specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <controller_manager/controller_manager.h>
#include <ros/ros.h>

#include "libbpl_protocol/constants.h"
#include "rsa_bravo_driver/bravo_robot_hw.h"

namespace rsa_bravo_driver {
using libbpl_protocol::PacketTypes;

class BravoInterfaceOnly {
 public:
  enum class BravoState { Uninitialized, Initialized, Unknown };
  enum class EnableRequest { None, EnableRequested, DisableRequested };

  BravoInterfaceOnly(ros::NodeHandle &nh, ros::NodeHandle &pnh)
      : name_("hardware_interface"),
        state_(BravoState::Unknown),
        monitor_mode_(false),
        nh_(nh),
        enable_requested_(EnableRequest::None) {
    ROS_INFO("Initializing BravoInterfaceOnly!");

    // Load rosparams
    std::size_t error = 0;

    // Code API of rosparam_shortcuts:
    // http://docs.ros.org/en/noetic/api/rosparam_shortcuts/html/namespacerosparam__shortcuts.html#aa6536fe0130903960b1de4872df68d5d
    error += !rosparam_shortcuts::get(name_, pnh, "control_loop_rate",
                                      control_loop_rate_);
    error +=
        !rosparam_shortcuts::get(name_, pnh, "arm_poll_freq", arm_poll_freq_);
    error +=
        !rosparam_shortcuts::get(name_, pnh, "joint_expiration_microseconds",
                                 joint_expiration_microseconds_);

    arm_ip_address_ = pnh.param<std::string>(
        "arm_ip_address", libbpl_protocol::DefaultBravoComputeIP);
    arm_udp_port_ = pnh.param("arm_port", 6789);

    monitor_mode_ = pnh.param("monitor_mode", false);

    rosparam_shortcuts::shutdownIfError(name_, error);

    // Set up control loop
    const double ControlLoopTimerPeriod = 1;
    //        (control_loop_rate_ > 0 ? (1.0 / control_loop_rate_) : 10.0);
    control_loop_timer_ = nh.createTimer(ros::Duration(ControlLoopTimerPeriod),
                                         &BravoInterfaceOnly::update, this);
  }

  ~BravoInterfaceOnly() {}

  void connectToArm(void) {
    // n.b. This will destroy/drop the existing socket and open a new
    //      connection
    if (monitor_mode_) {
      bravo_interface_ = std::make_shared<ReadOnlyBravoInterface>(
          nh_, arm_ip_address_, arm_udp_port_);
    } else {
      bravo_interface_ = std::make_shared<BravoInterfacePolled>(
          nh_, arm_ip_address_, arm_udp_port_, arm_poll_freq_,
          joint_expiration_microseconds_);
    }

    state_ = BravoState::Uninitialized;
    last_arm_init_ = std::chrono::system_clock::now();

    bravo_interface_->disable();
  }

  // This function runs at control_loop_rate.  For now, the whole state machine
  // is implemented in this function for visibility
  void update(const ros::TimerEvent &e) {
    if (!bravo_interface_) {
      connectToArm();
      return;
    }

    // Always wait a period after initializating the arm
    // \todo{amarburg} Make this configurable
    auto const time_since_init =
        std::chrono::system_clock::now() - last_arm_init_;
    if (time_since_init < std::chrono::milliseconds(1000)) {
      return;
    }

    if (state_ == BravoState::Uninitialized) {
      ROS_INFO(
          "** bravo_interface standoff period elapsed;   now monitoring arm "
          "comms");
    }

    if (!bravo_interface_->areCommsGood()) {
      ROS_WARN(
          "No communication from arm, attempting to restart communications");
      connectToArm();
      return;
    }

    // If you've made it this far, communication with the arm is good
    if (state_ == BravoState::Uninitialized) {
      state_ = BravoState::Initialized;
      bravo_interface_->disable();
    }

    bool should_reset = false;
    if (enable_requested_ == EnableRequest::DisableRequested) {
      ROS_WARN("Disable requested ... disabling arm");
      enable_requested_ = EnableRequest::None;
      bravo_interface_->disable();

    } else if ((!bravo_interface_->isEnabled()) &&
               (enable_requested_ == EnableRequest::EnableRequested)) {
      // Enable
      bravo_interface_->enable();
      enable_requested_ = EnableRequest::None;
      should_reset = true;
    }
  }

  bool enableSrvCallback(rsa_bravo_msgs::EnableBravoRequest &req,
                         rsa_bravo_msgs::EnableBravoResponse &res) {
    if (req.enable) {
      enable_requested_ = EnableRequest::EnableRequested;
    } else {
      enable_requested_ = EnableRequest::DisableRequested;
    }

    return true;
  }

 protected:
  std::string name_;
  std::string arm_ip_address_;
  unsigned int arm_udp_port_;
  bool monitor_mode_;

  ros::NodeHandle nh_;

  std::shared_ptr<BravoInterface> bravo_interface_;
  std::chrono::time_point<std::chrono::system_clock> last_arm_init_;

  BravoState state_;

  ros::Timer control_loop_timer_;
  double control_loop_rate_;

  ros::ServiceServer enable_bravo_server_;

  EnableRequest enable_requested_;

  // heartbeat frequency to query sensors. Cast to a uint8 later.
  double arm_poll_freq_ = 0.0;

  int joint_expiration_microseconds_;  // max allowed time since last update
};

}  // namespace rsa_bravo_driver

int main(int argc, char **argv) {
  // Initialize the ROS node
  ros::init(argc, argv, "rsa_bravo_interface_only");
  ros::NodeHandle nh;
  ros::NodeHandle pnh("~");

  rsa_bravo_driver::BravoInterfaceOnly bravo(nh, pnh);
  ros::spin();

  return 0;
}
