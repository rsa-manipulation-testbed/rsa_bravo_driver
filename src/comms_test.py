#!/usr/bin/env python3
#
# Copyright 2023 University of Washington, Oregon State University
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
# may be used to endorse or promote products derived from this software without
# specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import time
import socket
from bplprotocol import BPLProtocol, PacketID, PacketReader

MANIPULATOR_IP_ADDRESS = "192.168.2.3"
MANIPULATOR_PORT = 6789

DEVICE_IDS = [1, 2, 3, 4, 5, 6, 7]

REQUEST_FREQUENCY = 100

if __name__ == "__main__":

    manipulator_address = (MANIPULATOR_IP_ADDRESS, MANIPULATOR_PORT)
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(0)

    pr = PacketReader()

    # Device_id 0XFF is broadcast that is received by all devices.
    request_packet = BPLProtocol.encode_packet(
        0x01, PacketID.REQUEST, bytes([PacketID.POSITION])
    )

    positions = ["-"] * len(DEVICE_IDS)

    next_req_time = time.time() + (1 / REQUEST_FREQUENCY)
    sent_time = time.time()

    while True:

        try:
            recv_bytes, address = sock.recvfrom(4096)
        except Exception:
            recv_bytes = b""

        if recv_bytes:
            packets = pr.receive_bytes(recv_bytes)
            print("elapsed time: {}".format(time.time() - sent_time))
            for device_id, packet_id, data in packets:
                print("device: {}".format(device_id))
                if packet_id == PacketID.POSITION:

                    if device_id in DEVICE_IDS:
                        position = BPLProtocol.decode_floats(data)[0]

                        idx = DEVICE_IDS.index(device_id)

                        positions[idx] = f"{position:.2f}"

                        print_string = f"{time.perf_counter():.3f}| Positions: "
                        for dev_id, position in zip(DEVICE_IDS, positions):
                            print_string += f"{dev_id}: {position}, "
                        print(print_string)

        if time.time() >= next_req_time:
            next_req_time += 1 / REQUEST_FREQUENCY
            sock.sendto(request_packet, manipulator_address)
            sent_time = time.time()
