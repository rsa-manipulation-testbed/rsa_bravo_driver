// Copyright 2023 University of Washington, Oregon State University
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
// may be used to endorse or promote products derived from this software without
// specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "rsa_bravo_driver/bravo_robot_hw.h"

#include <std_msgs/Empty.h>
#include <std_msgs/Int32.h>

#include <chrono>
#include <iomanip>
#include <limits>

#include "libbpl_protocol/constants.h"

namespace rsa_bravo_driver {
using libbpl_protocol::PacketTypes;

BravoRobotHw::BravoRobotHw(ros::NodeHandle &nh, ros::NodeHandle &pnh,
                           urdf::Model *urdf_model)
    : nh_(nh),
      pnh_(pnh),
      name_("hardware_interface"),
      state_(BravoState::Unknown),
      arm_control_mode_(ControlMode::NoController),
      gripper_control_mode_(ControlMode::NoController),
      monitor_mode_(false),
      enable_requested_(EnableRequest::None) {
  ROS_DEBUG("Initializing node!");

  // Initialization of the robot's resources (joints, sensors, actuators) and
  // interfaces can be done here or inside init().
  // E.g. parse the URDF for joint names & interfaces, then initialize them

  // Check if the URDF model needs to be loaded
  if (urdf_model == NULL)
    loadURDF(nh, "robot_description");
  else
    urdf_model_ = urdf_model;

  // Load rosparams
  std::size_t error = 0;

  // Code API of rosparam_shortcuts:
  // http://docs.ros.org/en/noetic/api/rosparam_shortcuts/html/namespacerosparam__shortcuts.html#aa6536fe0130903960b1de4872df68d5d
  error += !rosparam_shortcuts::get(name_, pnh_, "hardware_interface/joints",
                                    joint_names_);
  error += !rosparam_shortcuts::get(name_, pnh_, "control_loop_rate",
                                    control_loop_rate_);
  error +=
      !rosparam_shortcuts::get(name_, pnh, "arm_poll_freq", arm_poll_freq_);
  error +=
      !rosparam_shortcuts::get(name_, pnh_, "joint_expiration_microseconds",
                               joint_expiration_microseconds_);
  error += !rosparam_shortcuts::get(name_, pnh_, "gripper_current_lower_lim",
                                    gripper_current_lower_lim_);
  error += !rosparam_shortcuts::get(name_, pnh_, "gripper_current_upper_lim",
                                    gripper_current_upper_lim_);

  error += !rosparam_shortcuts::get(name_, pnh_, "deadband_epsilon_rad",
                                    deadband_epsilon_rad_);

  monitor_mode_ = pnh.param("monitor_mode", false);

  arm_ip_address_ = pnh_.param<std::string>(
      "arm_ip_address", libbpl_protocol::DefaultBravoComputeIP);
  arm_udp_port_ = pnh_.param("arm_port", 6789);

  rosparam_shortcuts::shutdownIfError(name_, error);

  ROS_INFO_STREAM("Initialized RobotHW!");
}

BravoRobotHw::~BravoRobotHw() {}

bool BravoRobotHw::init() {
  ROS_INFO("Initializing Bravo Hardware Interface ...");

  enable_bravo_server_ = nh_.advertiseService(
      "enable_bravo", &BravoRobotHw::enableSrvCallback, this);

  num_joints_ = joint_names_.size();

  joint_position_lower_limits_.resize(num_joints_, 0.0);
  joint_position_upper_limits_.resize(num_joints_, 0.0);

  ROS_INFO("Number of joints: %d", (int)num_joints_);

  for (unsigned int i = 0; i < num_joints_; i++) {
    // Create a JointStateHandle for each joint and register them with the
    // JointStateInterface.
    hardware_interface::JointStateHandle joint_state_handle(
        joint_names_[i], &joint_positions_[i], &joint_velocities_[i],
        &joint_efforts_[i]);
    joint_state_interface_.registerHandle(joint_state_handle);

    {
      // Create a JointHandle (read and write) for each controllable joint
      // using the read-only joint handles within the JointStateInterface and
      // register them with the JointPositionInterface.
      hardware_interface::JointHandle joint_pos_handle(
          joint_state_handle, &joint_position_commands_[i]);
      position_joint_interface_.registerHandle(joint_pos_handle);

      // Load the joint position limits from the URDF
      registerJointPositionLimitsFromURDF(joint_pos_handle, i);
    }

    {
      hardware_interface::JointHandle joint_vel_handle(
          joint_state_handle, &joint_velocity_commands_[i]);
      velocity_joint_interface_.registerHandle(joint_vel_handle);

      // Check if there are joint velocity limits from config/bravo_control.yaml
      joint_limits_interface::JointLimits limits;
      if (joint_limits_interface::getJointLimits(joint_names_[i], nh_,
                                                 limits)) {
        if (limits.has_position_limits) {
          ROS_INFO_STREAM("Joint " << joint_names_[i] << " has velocity limit "
                                   << limits.max_velocity);
          joint_limits_interface::VelocityJointSaturationHandle
              jointVelLimitsHandle(joint_vel_handle, limits);
          vel_jnt_sat_interface_.registerHandle(jointVelLimitsHandle);
        }
      } else {
        ROS_WARN_STREAM("Unable to find joint velocity limit for "
                        << joint_names_[i]);
      }
    }

    // Initialize joint states with nan values (for async) and 0 values (for all
    // others)
    joint_positions_[i] = 0.0;
    joint_velocities_[i] = 0.0;
    joint_efforts_[i] = 0.0;

    joint_position_commands_[i] = 0.0;
    joint_velocity_commands_[i] = 0.0;
  }

  // Register the JointStateInterface containing the read only joints
  // with this robot's hardware_interface::RobotHW.
  registerInterface(&joint_state_interface_);

  // Register the JointVelocityInterface containing the read/write joints
  // with this robot's hardware_interface::RobotHW.
  registerInterface(&position_joint_interface_);
  registerInterface(&velocity_joint_interface_);

  controller_manager_.reset(
      new controller_manager::ControllerManager(this, nh_));
  connectToArm();

  // Set up control loop
  const double ControlLoopTimerPeriod =
      (control_loop_rate_ > 0 ? (1.0 / control_loop_rate_) : 10.0);
  control_loop_timer_ = nh_.createTimer(ros::Duration(ControlLoopTimerPeriod),
                                        &BravoRobotHw::update, this);

  return true;
}

void BravoRobotHw::connectToArm(void) {
  // n.b. This will destroy/drop the existing socket and open a new
  //      connection

  if (monitor_mode_) {
    bravo_interface_ = std::make_shared<ReadOnlyBravoInterface>(
        nh_, arm_ip_address_, arm_udp_port_);
  } else {
    bravo_interface_ = std::make_shared<BravoInterfacePolled>(
        nh_, arm_ip_address_, arm_udp_port_, arm_poll_freq_,
        joint_expiration_microseconds_);
  }

  state_ = BravoState::Uninitialized;
  last_arm_init_ = std::chrono::system_clock::now();

  bravo_interface_->disable();

  // Set the current limits for the gripper
  // \todo{amarburg}  Right now, we set the limits but don't verify that
  //                  they have been set successfully
  bravo_interface_->setCurrentLimits(0x01, gripper_current_lower_lim_,
                                     gripper_current_upper_lim_);

  // Check that the current limit was successfully updated
  // sent = Packet(PacketTypes::REQUEST, 0x01);  // 0x01 is arm
  // sent.push_back(PacketTypes::CURRENT_LIMITS);
  // bravo_interface_->send(sent);
}

// This function runs at control_loop_rate.  For now, the whole state machine
// is implemented in this function for visibility
void BravoRobotHw::update(const ros::TimerEvent &e) {
  if (!bravo_interface_) {
    connectToArm();
    return;
  }

  // Always wait a period after initializating the arm
  // \todo{amarburg} Make this configurable
  auto const time_since_init =
      std::chrono::system_clock::now() - last_arm_init_;
  if (time_since_init < std::chrono::milliseconds(1000)) {
    return;
  }

  if (state_ == BravoState::Uninitialized) {
    ROS_INFO(
        "** bravo_interface standoff period elapsed;   now monitoring arm "
        "comms");
  }

  if (!bravo_interface_->areCommsGood()) {
    ROS_WARN("No communication from arm, attempting to restart communications");
    connectToArm();
    return;
  }

  // If you've made it this far, communication with the arm is good
  if (state_ == BravoState::Uninitialized) {
    state_ = BravoState::Initialized;
    bravo_interface_->disable();
  }
  const ros::Duration dt = e.current_real - e.last_real;
  this->read(e.current_real, dt);

  bool should_reset = false;
  if (enable_requested_ == EnableRequest::DisableRequested) {
    ROS_WARN("Disabling arm");
    enable_requested_ = EnableRequest::None;
    bravo_interface_->disable();

  } else if ((!bravo_interface_->isEnabled()) &&
             (enable_requested_ == EnableRequest::EnableRequested)) {
    // Enable
    ROS_WARN("Enabling arm");
    bravo_interface_->enable();
    enable_requested_ = EnableRequest::None;
    should_reset = true;
  }

  // If needed, its possible to define transmissions in software by calling
  // the transmission_interface::ActuatorToJointPositionInterface::propagate()
  // after reading the joint states.
  controller_manager_->update(e.current_real, dt, should_reset);

  if (bravo_interface_->isEnabled()) {
    // In case of software transmissions, use
    // transmission_interface::JointToActuatorEffortHandle::propagate()
    // to convert from the joint space to the actuator space.

    this->write(e.current_real, dt);
  }
}

void BravoRobotHw::read(const ros::Time &time, const ros::Duration &period) {
  if (!bravo_interface_) return;

  for (std::size_t i = 0; i < num_joints_; ++i) {
    joint_positions_[i] = bravo_interface_->position(i);
    joint_velocities_[i] = bravo_interface_->velocity(i);
    joint_efforts_[i] = bravo_interface_->effort(i);
  }
}

void BravoRobotHw::write(const ros::Time &time, const ros::Duration &period) {
  if (!bravo_interface_ || !bravo_interface_->isEnabled()) return;

  // Enforce position and velocity limits
  pos_jnt_sat_interface_.enforceLimits(period);
  vel_jnt_sat_interface_.enforceLimits(period);

  // convert linear actuator (gripper) command from meters to mm; (or mm/s)
  // all other joints operate natively in radians
  if (gripper_control_mode_ == ControlMode::PositionController) {
    float command = static_cast<float>(joint_position_commands_[0]);
    ROS_DEBUG_STREAM_NAMED(
        name_, "Commanding gripper to position " << command << " m");
    command *= 1000;
    bravo_interface_->sendPosition(1, command);
  } else if (gripper_control_mode_ == ControlMode::VelocityController) {
    float command = static_cast<float>(joint_velocity_commands_[0]);
    ROS_DEBUG_STREAM_NAMED(name_,
                           "Commanding gripper velocity " << command << " m/s");
    command *= 1000;
    bravo_interface_->sendVelocity(1, command);
  }

  if (arm_control_mode_ == ControlMode::PositionController) {
    for (std::size_t i = 1; i < num_joints_; ++i) {
      float command = static_cast<float>(joint_position_commands_[i]);

      ROS_DEBUG_STREAM_NAMED(
          name_, "Sending joint " << i + 1 << " to position " << command);
      bravo_interface_->sendPosition(i + 1, command);
    }
  } else if (arm_control_mode_ == ControlMode::VelocityController) {
    for (std::size_t i = 1; i < num_joints_; ++i) {
      float command = static_cast<float>(joint_velocity_commands_[i]);

      ROS_DEBUG_STREAM_NAMED(
          name_, "Sending joint " << i + 1 << " to velocity" << command);
      bravo_interface_->sendVelocity(i + 1, command);
    }
  } else {
    ROS_WARN_STREAM_THROTTLE_NAMED(1, name_,
                                   "Arm has **NO ACTIVE CONTROLLER**");
  }
}

void BravoRobotHw::doSwitch(
    const std::list<hardware_interface::ControllerInfo> &start_list,
    const std::list<hardware_interface::ControllerInfo> &stop_list) {
  ROS_WARN_STREAM("In BravoRobotHw::doSwitch, starting "
                  << start_list.size() << ", stopping " << stop_list.size());

  const std::string arm_match_string = "bravo_axis_";
  const std::string gripper_match_string = arm_match_string + "a";

  for (auto const &c : stop_list) {
    ROS_WARN_STREAM("--- Stopping controller " << c.name << " : " << c.type);

    for (auto const &r : c.claimed_resources) {
      ROS_WARN_STREAM("---   Hardware resources : " << r.hardware_interface);

      for (auto const &resource : r.resources) {
        ROS_WARN_STREAM("---     Resource : " << resource);

        if (resource.compare(gripper_match_string) == 0) {
          ROS_WARN_STREAM("Stopping controller " << c.name << " for gripper");
          gripper_control_mode_ = ControlMode::NoController;

        } else if (resource.compare(0, arm_match_string.size(),
                                    arm_match_string) == 0) {
          ROS_WARN_STREAM("Stopping controller " << c.name << " for arm");
          arm_control_mode_ = ControlMode::NoController;

        } else {
          ROS_WARN_STREAM("Don't understand the joint \"" << c.name << "\"");
        }
      }
    }
  }

  for (auto const &c : start_list) {
    ROS_WARN_STREAM("+++ Starting controller " << c.name << " : " << c.type);

    for (auto const &r : c.claimed_resources) {
      ROS_WARN_STREAM("+++   Hardware resources : " << r.hardware_interface);
      for (auto const &resource : r.resources) {
        ROS_WARN_STREAM("+++     Resource : " << resource);

        // Don't like this fixed string
        if (resource.compare(gripper_match_string) == 0) {
          if (c.type.find("velocity_controllers") == 0) {
            ROS_WARN("Setting gripper to velocity control");
            gripper_control_mode_ = ControlMode::VelocityController;
          } else if (c.type.find("position_controllers") == 0) {
            ROS_WARN("Setting gripper to position control");
            gripper_control_mode_ = ControlMode::PositionController;
          } else {
            ROS_WARN("Setting gripper to **NO CONTROLLER**");
            gripper_control_mode_ = ControlMode::NoController;
          }

        } else if (resource.compare(0, arm_match_string.size(),
                                    arm_match_string) == 0) {
          if (c.type.find("velocity_controllers") == 0) {
            ROS_WARN("Setting arm to velocity control");
            arm_control_mode_ = ControlMode::VelocityController;
          } else if (c.type.find("position_controllers") == 0) {
            ROS_WARN("Setting arm to position control");
            arm_control_mode_ = ControlMode::PositionController;
          } else {
            ROS_WARN("Setting arm to **NO CONTROLLER**");
            arm_control_mode_ = ControlMode::NoController;
          }

        } else {
          ROS_WARN_STREAM("Don't understand the joint \"" << c.name << "\"");
        }
      }
    }
  }
}

bool BravoRobotHw::enableSrvCallback(rsa_bravo_msgs::EnableBravoRequest &req,
                                     rsa_bravo_msgs::EnableBravoResponse &res) {
  if (req.enable) {
    enable_requested_ = EnableRequest::EnableRequested;
  } else {
    enable_requested_ = EnableRequest::DisableRequested;
  }

  return true;
}

void BravoRobotHw::loadURDF(const ros::NodeHandle &nh, std::string param_name) {
  std::string urdf_string;
  urdf_model_ = new urdf::Model();
  // search and wait for robot_description on param server
  while (urdf_string.empty() && ros::ok()) {
    std::string search_param_name;
    if (nh.searchParam(param_name, search_param_name)) {
      ROS_INFO_STREAM_NAMED(
          name_, "Waiting for model URDF on the ROS param server at location: "
                     << nh.getNamespace() << search_param_name);
      nh.getParam(search_param_name, urdf_string);
    } else {
      ROS_INFO_STREAM_NAMED(
          name_, "Waiting for model URDF on the ROS param server at location: "
                     << nh.getNamespace() << param_name);
      nh.getParam(param_name, urdf_string);
    }

    usleep(100000);
  }

  if (!urdf_model_->initString(urdf_string))
    ROS_ERROR_STREAM_NAMED(name_, "Unable to load URDF model");
  else
    ROS_DEBUG_STREAM_NAMED(name_, "Received URDF from param server");
}

void BravoRobotHw::registerJointPositionLimitsFromURDF(
    const hardware_interface::JointHandle &joint_handle_position,
    std::size_t joint_id) {
  // Default values
  joint_position_lower_limits_[joint_id] = -std::numeric_limits<double>::max();
  joint_position_upper_limits_[joint_id] = std::numeric_limits<double>::max();

  // Limits datastructures
  joint_limits_interface::JointLimits joint_limits;  // Position
  bool has_joint_limits = false;

  // Get limits from URDF
  if (urdf_model_ == NULL) {
    ROS_WARN_STREAM_NAMED(name_,
                          "No URDF model loaded, unable to get joint limits");
    return;
  }

  // Get limits from URDF
  urdf::JointConstSharedPtr urdf_joint =
      urdf_model_->getJoint(joint_names_[joint_id]);

  // Get main joint limits
  if (urdf_joint == NULL) {
    ROS_ERROR_STREAM_NAMED(name_,
                           "URDF joint not found " << joint_names_[joint_id]);
    return;
  }

  // Get limits from URDF
  if (joint_limits_interface::getJointLimits(urdf_joint, joint_limits)) {
    has_joint_limits = true;
    ROS_INFO_STREAM_NAMED(name_, "Joint " << joint_names_[joint_id]
                                          << " has URDF position limits ["
                                          << joint_limits.min_position << ", "
                                          << joint_limits.max_position << "]");
  } else {
    if (urdf_joint->type != urdf::Joint::CONTINUOUS)
      ROS_WARN_STREAM_NAMED(name_, "Joint " << joint_names_[joint_id]
                                            << " does not have a URDF "
                                               "position limit");
  }

  // Quit we we haven't found any limits in URDF
  if (!has_joint_limits) {
    return;
  }

  // Copy position limits if available
  if (joint_limits.has_position_limits) {
    // Slighly reduce the joint limits to prevent floating point errors
    joint_limits.min_position += std::numeric_limits<double>::epsilon();
    joint_limits.max_position -= std::numeric_limits<double>::epsilon();

    joint_position_lower_limits_[joint_id] = joint_limits.min_position;
    joint_position_upper_limits_[joint_id] = joint_limits.max_position;
  }

  // Use saturation limits
  const joint_limits_interface::PositionJointSaturationHandle
      sat_handle_position(joint_handle_position, joint_limits);
  pos_jnt_sat_interface_.registerHandle(sat_handle_position);
}
};  // namespace rsa_bravo_driver
