// Copyright 2023 University of Washington, Oregon State University
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
// may be used to endorse or promote products derived from this software without
// specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <rsa_bravo_driver/bravo_interface.h>
#include <rsa_bravo_msgs/BravoFloat.h>
#include <rsa_bravo_msgs/BravoLimits.h>
#include <rsa_bravo_msgs/BravoMode.h>

namespace rsa_bravo_driver {
using libbpl_protocol::PacketTypes;

const double nan_double_value = std::numeric_limits<double>::quiet_NaN();
const long long min_ll_value = std::numeric_limits<long long>::min();

long long microsecond_time() {
  // get time in microseconds since epoch
  auto now = std::chrono::high_resolution_clock::now();
  auto duration = now.time_since_epoch();
  return std::chrono::duration_cast<std::chrono::microseconds>(duration)
      .count();
}

BravoInterface::BravoInterface(ros::NodeHandle &nh,
                               const std::string &ipAddress, const int port)
    : io_thread_() {
  mode_pub_ = nh.advertise<rsa_bravo_msgs::BravoMode>("bravo/mode", 10);
  position_pub_ =
      nh.advertise<rsa_bravo_msgs::BravoFloat>("bravo/position", 10);
  velocity_pub_ =
      nh.advertise<rsa_bravo_msgs::BravoFloat>("bravo/velocity", 10);
  current_pub_ = nh.advertise<rsa_bravo_msgs::BravoFloat>("bravo/current", 10);
  temperature_pub_ =
      nh.advertise<rsa_bravo_msgs::BravoFloat>("bravo/temperature", 10);
  voltage_pub_ = nh.advertise<rsa_bravo_msgs::BravoFloat>("bravo/voltage", 10);

  position_limits_pub_ =
      nh.advertise<rsa_bravo_msgs::BravoLimits>("bravo/joint_limits", 10);
  velocity_limits_pub_ =
      nh.advertise<rsa_bravo_msgs::BravoLimits>("bravo/velocity_limits", 10);
  current_limits_pub_ =
      nh.advertise<rsa_bravo_msgs::BravoLimits>("bravo/current_limits", 10);

  for (unsigned int i = 0; i < NUM_JOINTS; i++) {
    joint_positions_[i] = nan_double_value;
    joint_update_times_[i] = min_ll_value;  // this is just 0
    joint_velocities_[i] = 0.0;
    joint_efforts_[i] = 0.0;
  }

  // \todo{}  Make this interval configurable
  statistics_timer_ = nh.createTimer(ros::Duration(1.0),
                                     &BravoInterface::statisticsCallback, this);

  bpl_client_ = std::make_shared<libbpl_protocol::AsynchronousClient>(
      io_thread_.context(), ipAddress, port);
  bpl_client_->add_callback(std::bind(&BravoInterface::packetRxCallback, this,
                                      std::placeholders::_1));

  io_thread_.start();
}

BravoInterface::~BravoInterface() {
  halt_bravo();
  io_thread_.stop();
  io_thread_.join();
}

void BravoInterface::halt_bravo() {
  // Set all joints to zero velocity
  for (std::size_t i = 1; i <= NUM_JOINTS; ++i) {
    sendVelocity(i, 0);
  }

  disable();  // set actuators to MODE_DISABLE
}

void BravoInterface::packetRxCallback(libbpl_protocol::Packet packet) {
  // ROS_INFO_STREAM("Got packet type "
  //                           << std::setw(2) << std::setfill('0') << std::hex
  //                           << static_cast<int>(packet.type()) << " from
  //                           device "
  //                           << static_cast<int>(packet.deviceId()) <<
  //                           std::dec);

  packets_from_arm_.packetRx(packet);

  // deviceID 1 through 7 corresponds to joints A (jaw) through F (base
  // swivel)
  //
  const uint8_t idx = packet.deviceId() - 1;

  if (idx < 0 || idx >= NUM_JOINTS) {
    ROS_DEBUG_STREAM_THROTTLE(0.1,
                              "Received packet from unexpected device address "
                                  << idx + 1 << ", ignoring...");
    return;
  }

  const long long now = microsecond_time();

  if (packet.type() == PacketTypes::POSITION) {
    joint_update_times_[idx] = now;
    joint_positions_[idx] = packet.pop_front<float>();
    if (idx == 0) {
      // The linear actuator on the gripper (joint 1) returns mm
      // Convert to m
      joint_positions_[idx] /= 1000.0;
    }
    publishBravoFloatMsg(position_pub_, packet.deviceId(),
                         joint_positions_[idx]);
  } else if (packet.type() == PacketTypes::VELOCITY) {
    joint_update_times_[idx] = now;
    joint_velocities_[idx] = packet.pop_front<float>();
    if (idx == 0) {
      // The linear actuator on the gripper (joint 1) returns mm/s
      // Convert to m/s
      joint_velocities_[idx] /= 1000.0;
    }
    publishBravoFloatMsg(velocity_pub_, packet.deviceId(),
                         joint_velocities_[idx]);
  } else if (packet.type() == PacketTypes::CURRENT) {
    joint_update_times_[idx] = now;
    joint_efforts_[idx] = packet.pop_front<float>();
    publishBravoFloatMsg(current_pub_, packet.deviceId(), joint_efforts_[idx]);
  } else if (packet.type() == PacketTypes::TEMPERATURE) {
    publishBravoFloatMsg(temperature_pub_, packet.deviceId(),
                         packet.pop_front<float>());
  } else if (packet.type() == PacketTypes::VOLTAGE) {
    publishBravoFloatMsg(voltage_pub_, packet.deviceId(),
                         packet.pop_front<float>());
  } else if (packet.type() == PacketTypes::MODE) {
    auto mode = packet.pop_front<libbpl_protocol::Mode>();

    rsa_bravo_msgs::BravoMode msg;
    msg.header.stamp = ros::Time::now();
    msg.joint = packet.deviceId();
    msg.mode = static_cast<int8_t>(mode);
    mode_pub_.publish(msg);
  } else if (packet.type() == PacketTypes::POSITION_LIMITS) {
    publishBravoLimitsMsg(position_limits_pub_, packet);
  } else if (packet.type() == PacketTypes::VELOCITY_LIMITS) {
    publishBravoLimitsMsg(velocity_limits_pub_, packet);
  } else if (packet.type() == PacketTypes::CURRENT_LIMITS) {
    publishBravoLimitsMsg(current_limits_pub_, packet);
  } else {
    ROS_DEBUG_STREAM_THROTTLE(
        0.1, "Got unexpected packet type "
                 << std::setw(2) << std::setfill('0') << std::hex
                 << static_cast<int>(packet.type()) << " from device "
                 << static_cast<int>(packet.deviceId()) << std::dec);
  }
}

void BravoInterface::statisticsCallback(const ros::TimerEvent &e) {
  // If uncommented, print verbose debugging output to stderr
  // packets_from_arm_.dump();

  packets_from_arm_.reset();
}

void BravoInterface::publishBravoFloatMsg(ros::Publisher &pub, int joint,
                                          float val) {
  // Send float message
  rsa_bravo_msgs::BravoFloat msg;
  msg.header.stamp = ros::Time::now();
  msg.joint = joint;
  msg.val = val;
  pub.publish(msg);
}

void BravoInterface::publishBravoLimitsMsg(ros::Publisher &pub,
                                           libbpl_protocol::Packet packet) {
  // Send limit message
  rsa_bravo_msgs::BravoLimits msg;
  msg.header.stamp = ros::Time::now();
  msg.joint = packet.deviceId();
  msg.maximum = packet.pop_front<float>();
  msg.minimum = packet.pop_front<float>();
  pub.publish(msg);
}

void BravoInterface::enable() {
  is_enabled_ = true;

  // ~~ Uncomment below to send "enable" command to the broadcast address
  // libbpl_protocol::Packet pkt_out(PacketTypes::MODE,
  // libbpl_protocol::BroadcastDeviceId); pkt_out.push_back(Mode::MODE_STANDBY);
  // send(pkt_out);

  // ~~~ Below sends enables sequentially to each joint with a pause between
  //     packets
  const unsigned int wait_usecs = 25000;
  for (std::size_t i = 1; i <= NUM_JOINTS; ++i) {
    libbpl_protocol::Packet pkt_out(PacketTypes::MODE, i);
    pkt_out.push_back(libbpl_protocol::Mode::MODE_STANDBY);
    send(pkt_out);

    usleep(wait_usecs);
  }
}

void BravoInterface::disable() {
  // Set all joints to MODE_DISABLE
  is_enabled_ = false;
  libbpl_protocol::Packet pkt_out(PacketTypes::MODE,
                                  libbpl_protocol::BroadcastDeviceId);
  pkt_out.push_back(libbpl_protocol::Mode::MODE_DISABLE);

  send(pkt_out);
}

void BravoInterface::setCurrentLimits(uint8_t device_id, double lower_limit,
                                      double upper_limit) {
  auto pkt_out(Packet(PacketTypes::CURRENT_LIMITS, device_id));
  pkt_out.push_back<float>(upper_limit, lower_limit);
  send(pkt_out);
}

void BravoInterface::sendPosition(uint8_t device_id, float position) {
  Packet pkt_out(PacketTypes::POSITION, device_id);
  pkt_out.push_back<float>(position);
  send(pkt_out);
}

void BravoInterface::sendVelocity(uint8_t device_id, float velocity) {
  Packet pkt_out(PacketTypes::VELOCITY, device_id);
  pkt_out.push_back<float>(velocity);
  send(pkt_out);
}

//===

ReadOnlyBravoInterface::ReadOnlyBravoInterface(ros::NodeHandle &nh,
                                               const std::string &ipAddress,
                                               const int port)
    : BravoInterface(nh, ipAddress, port) {
  // Send just one request to prime the watchdog UDP port
  bpl_client_->request(libbpl_protocol::BroadcastDeviceId, {PacketTypes::MODE});
}

//====

ReadWriteBravoInterface::ReadWriteBravoInterface(ros::NodeHandle &nh,
                                                 const std::string &ipAddress,
                                                 const int port,
                                                 long long joint_expiration)
    : BravoInterface(nh, ipAddress, port),
      joint_expiration_microseconds_(joint_expiration) {}

void ReadWriteBravoInterface::send(const libbpl_protocol::Packet &v) {
  bpl_client_->send(v);
}

bool ReadWriteBravoInterface::areCommsGood(void) const {
  auto now_us = microsecond_time();

  bool joints_are_valid = true;
  for (unsigned int i = 0; i < NUM_JOINTS; i++) {
    if (now_us - joint_update_times_[i] > joint_expiration_microseconds_) {
      ROS_INFO_STREAM("Last position, velocity or current from joint "
                      << i << " was " << now_us - joint_update_times_[i]
                      << " us ago");
      joints_are_valid = false;
    }

    if (std::isnan(joint_positions_[i])) {
      ROS_INFO_STREAM("Joint " << i << " is nan");
      joints_are_valid = false;
    }
  }

  return joints_are_valid;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
//

BravoInterfaceHeartbeat::BravoInterfaceHeartbeat(ros::NodeHandle &nh,
                                                 const std::string &ipAddress,
                                                 const int port,
                                                 int heartbeat_freq,
                                                 long long joint_expiration)
    : ReadWriteBravoInterface(nh, ipAddress, port, joint_expiration) {
  for (std::size_t i = 1; i <= NUM_JOINTS; ++i) {
    bpl_client_->setHeartbeatSet(
        i,
        {PacketTypes::POSITION, PacketTypes::VELOCITY, PacketTypes::CURRENT});
  }

  const uint8_t freq = (uint8_t)heartbeat_freq;  // Hz
  if (freq > 0) {
    for (std::size_t i = 1; i <= NUM_JOINTS; ++i) {
      bpl_client_->setHeartbeatFrequency(i,
                                         static_cast<uint8_t>(heartbeat_freq));
    }
  }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
//

// todo{}  Make these configurable
const float BravoInterfacePolled::PollMetadataPeriod = 0.25;
const float BravoInterfacePolled::PollLimitsPeriod = 5.0;

BravoInterfacePolled::BravoInterfacePolled(ros::NodeHandle &nh,
                                           const std::string &ipAddress,
                                           const int port, int poll_freq,
                                           long long joint_expiration)
    : ReadWriteBravoInterface(nh, ipAddress, port, joint_expiration) {
  poll_metadata_timer_ =
      nh.createTimer(ros::Duration(PollMetadataPeriod),
                     &BravoInterfacePolled::pollMetadataCallback, this);

  poll_limits_timer_ =
      nh.createTimer(ros::Duration(PollLimitsPeriod),
                     &BravoInterfacePolled::pollLimitsCallback, this);

  const double poll_period = (poll_freq > 0 ? (1.0 / poll_freq) : 1.0);
  poll_joints_timer_ =
      nh.createTimer(ros::Duration(poll_period),
                     &BravoInterfacePolled::pollJointsCallback, this);
}

// This is the "high rate" callback for critical dynamic information
void BravoInterfacePolled::pollJointsCallback(const ros::TimerEvent &e) {
  bpl_client_->request(
      libbpl_protocol::BroadcastDeviceId,
      {PacketTypes::POSITION, PacketTypes::VELOCITY, PacketTypes::CURRENT});
}

// This is the "low rate" callback for metainformation that may change
void BravoInterfacePolled::pollMetadataCallback(const ros::TimerEvent &e) {
  bpl_client_->request(
      libbpl_protocol::BroadcastDeviceId,
      {PacketTypes::MODE, PacketTypes::TEMPERATURE, PacketTypes::VOLTAGE});
}

// This is the "very slow rate" callback for metainformation that
// generally doesn't change
void BravoInterfacePolled::pollLimitsCallback(const ros::TimerEvent &e) {
  bpl_client_->request(
      libbpl_protocol::BroadcastDeviceId,
      {PacketTypes::POSITION_LIMITS, PacketTypes::VELOCITY_LIMITS,
       PacketTypes::CURRENT_LIMITS});
}

};  // namespace rsa_bravo_driver
